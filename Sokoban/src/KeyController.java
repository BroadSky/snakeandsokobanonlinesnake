import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
public class KeyController extends KeyAdapter {
    Model model;
    public KeyController(Model model) {
        this.model = model;
    }
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        int key = e.getKeyCode();
        switch (key) {
            case KeyEvent.VK_RIGHT:
                model.doStep(Direction.RIGHT,0);
                break;
            case KeyEvent.VK_LEFT:
                model.doStep(Direction.LEFT,0);

                break;
            case KeyEvent.VK_DOWN:
                model.doStep(Direction.DOWN,0);

                break;
            case KeyEvent.VK_UP:
                model.doStep(Direction.UP,0);
                break;
        }
    }
}

public interface Model {
    int DOT_SIZE = 16;
    int PORT = 8080;
    Player[] getPlayers();
    void doStep(Direction dirNowFS, int player);
    void setPlayers(Player[] player);
}

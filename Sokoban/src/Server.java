import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
public class Server implements Runnable {
    Socket socket;
    Model model;
    private ObjectInputStream dir;
    private Direction dirNowSS;
    public Server(Socket socket, Model model){
        this.socket = socket;
        this.model=model;
        try {
            dir = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public void run(){
        while(true) {
            try {
                dirNowSS = (Direction) dir.readObject();
                model.doStep(dirNowSS, 1);
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class Client implements Runnable {
    Socket socket;
    Model model;
    private Player[] players;
    private DataInputStream in;
    public Client(Socket socket, Model model){
        this.socket = socket;
        this.model = model;
        players = new Player[2];
        players = model.getPlayers();
        try {
            in = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        while(true){
            try {
                players[1].x = in.readInt();
                players[1].y = in.readInt();
                players[0].x = in.readInt();
                players[0].y = in.readInt();
                model.setPlayers(players);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
public class View extends JPanel {
    private Image dot1;
    private Image dot2;
    public Model model;
    LevelOne levelOne = new LevelOne(this);
    Socket socket;
    public View(String mode) throws InterruptedException {
        setPreferredSize(new Dimension(528, 512));
        setBackground(Color.CYAN);
        imageLoad();
        setFocusable(true);
        if(mode.equals("client")){
            while(socket==null){
                try{
                    socket = new Socket("localhost", model.PORT);
                }
                catch (IOException e) {
                    System.out.println("reconnecting...");
                    Thread.sleep(500);
                }
            }
            model = new ClientModel(this,socket,levelOne);
            new Thread(new Client(socket,model)).start();
        } else{
            try {
                socket = new ServerSocket(model.PORT).accept();
                model = new ServerModel(this, socket,levelOne);
                new Thread(new Server(socket,model)).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        addKeyListener(new KeyController(model));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(dot1, model.getPlayers()[0].x * model.DOT_SIZE, model.getPlayers()[0].y * model.DOT_SIZE, this);
        g.drawImage(dot2, model.getPlayers()[1].x * model.DOT_SIZE, model.getPlayers()[1].y * model.DOT_SIZE, this);
        levelOne.paintComponent(g);
        for(int x = 0; x<getWidth(); x+=model.DOT_SIZE) {
            g.fillRect(x, 0, model.DOT_SIZE, model.DOT_SIZE);
        }
        for(int x = 0; x<getWidth(); x+=model.DOT_SIZE) {
            g.fillRect(x, getHeight(), model.DOT_SIZE, model.DOT_SIZE);
        }
        for(int y = 0; y<getHeight(); y+=model.DOT_SIZE) {
            g.fillRect(0, y, model.DOT_SIZE, model.DOT_SIZE);
        }
        for(int y = 0; y<getHeight(); y+=model.DOT_SIZE) {
            g.fillRect(getWidth(), y, model.DOT_SIZE, model.DOT_SIZE);
        }
    }
    public void imageLoad() {
        ImageIcon dot1Icon = new ImageIcon("dot1.png");
        dot1 = dot1Icon.getImage();
        ImageIcon dot2Icon = new ImageIcon("dot2.png");
        dot2 = dot2Icon.getImage();
    }
    static class Window extends JFrame {
        public Window(String mode) throws InterruptedException {
            JFrame frame = new JFrame("Snake("+ mode+ ")");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.add(new View(mode));
            frame.pack();
            frame.setVisible(true);
        }

    }
}

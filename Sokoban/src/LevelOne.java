import javax.swing.*;
import java.awt.*;
public class LevelOne extends JPanel {
    private Image dot;
    View view;
    public LevelOne(View view){
        imageLoad();
        this.view=view;
    }
    protected void paintComponent(Graphics g) {
        for(int y = 0; y<view.getHeight(); y++){
            g.drawImage(dot, view.getWidth()/2, y, this);
        }
    }
    public void imageLoad() {
        dot = new ImageIcon("apple.png").getImage();
    }
    public int getBorderX(){
        return (view.getWidth()/2);
    }
}

import javax.swing.*;
import java.awt.*;
public class View extends JPanel   {
    private Image body;
    private Image apple;
    public Model model = new Model(this);
    private Controller controller = new Controller(model);
    public View() {
        setBackground(Color.CYAN);
        imageLoad();
        setFocusable(true);
        addKeyListener(controller);
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int[] xPos;
        int[] yPos;
        int[] appleX;
        int[] appleY;
        appleY = model.yApple();
        appleX = model.xApple();
        xPos = model.getSnakeX();
        yPos =model.getSnakeY();
        for (int i = 0; i < model.n; i++) {
            g.drawImage(apple, appleX[i] * model.DOT_SIZE, appleY[i] * model.DOT_SIZE, this);
        }
        for (int i = 0; i < model.snakeSize; i++) {
            g.drawImage(body, xPos[i], yPos[i], this);
        }
    }
    public void imageLoad() {
        ImageIcon appleIcon = new ImageIcon("apple.png");
        apple = appleIcon.getImage();
        ImageIcon bodyIcon = new ImageIcon("dot.png");
        body = bodyIcon.getImage();
    }
    static class Window extends JFrame {
        public Window(){
            setTitle("Snake");
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            setSize(496,519);
            setLocation(500,500);
            add(new View());
            setVisible(true);
            setResizable(true);
        }

    }

}

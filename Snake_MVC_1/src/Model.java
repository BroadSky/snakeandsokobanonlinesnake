import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
public class Model implements ActionListener  {
    public final int n = 3;
    public int DOT_SIZE = 16;
    int score = 0;
    public int xSize = 30;
    public int ySize = 30;
    public int[] appleX = new int[n];
    public int[] appleY = new int[n];
    private View view;
    private int[] xPos = new int[xSize * ySize];
    private int[] yPos = new int[ySize * xSize];
    public int snakeSize = 3;
    public boolean inGame = true;
    Direction dirNowFS = Direction.RIGHT;
    private Random random = new Random();
    public Model(View view){
        initGame();
        this.view=view;
    }
    public int[] getSnakeX() {
        return xPos;
    }

    public int[] getSnakeY() {
        return yPos;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (inGame) {
            move();
            checkApple();
            checkBorder();
        }
        view.repaint();
    }
    public void initGame() {
        Timer timer = new Timer(200, this);
        timer.start();
        for (int i = 0; i < snakeSize; i++) {
            xPos[i] = 0;
            yPos[i] = 240;
        }
        createApples();
    }

    public void setDirection(Direction dirNowFS) {
        this.dirNowFS = dirNowFS;
    }
    public Direction getDirection(){
        return dirNowFS;
    }

    public void move() {
        for (int i = snakeSize; i > 0; i--) {
            xPos[i] = xPos[i - 1];
            yPos[i] = yPos[i - 1];
        }
        switch (dirNowFS) {
            case LEFT:
                xPos[0] -= DOT_SIZE;
                break;
            case RIGHT:
                xPos[0] += DOT_SIZE;
                break;
            case UP:
                yPos[0] -=DOT_SIZE;
                break;
            case DOWN:
                yPos[0] += DOT_SIZE;
                break;
        }
    }
    public void createApples() {
        for (int i = 0; i < n; i++) {
            appleX[i] = random.nextInt(xSize);
            appleY[i] = random.nextInt(ySize);
        }
    }
    public void checkApple(){
        int f = 0;
        for (int i = 0; i < n; i++) {

            if (xPos[0] / DOT_SIZE == appleX[i] && yPos[0] / DOT_SIZE == appleY[i]) {
                snakeSize++;
                appleX[i] = -1;
                appleY[i] = -1;
                score++;
            }
        }
        for (int i = 0; i < n; i++) {
            if (appleX[i] == -1) {
                f++;
            }
            if (f == n) {
                score *= n;
                createApples();
            }
        }
    }
    public void checkBorder(){
        for (int i = snakeSize; i > 0; i--) {
            if (i > 4 && xPos[0] == xPos[i] && yPos[0] == yPos[i]) {

                inGame = false;
                break;
            }
        }
        if (xPos[0] >= xSize * DOT_SIZE | xPos[0] < -1 | yPos[0] >= ySize * DOT_SIZE | yPos[0] < 0) {
            inGame = false;
        }
    }
    public int[] xApple() {
        return appleX;
    }
    public int[] yApple() {
        return appleY;
    }
}

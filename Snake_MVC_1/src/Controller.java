import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
public class Controller extends KeyAdapter {
    Model model;
    public Controller(Model model) {
        this.model = model;
    }
    @Override
    public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_RIGHT) {
            switch (model.getDirection()) {
                case UP:
                    model.setDirection(Direction.RIGHT);
                    //out.writeInt(0);
                    break;
                case DOWN:
                    model.setDirection(Direction.LEFT);
                    break;
                //out.writeInt(1);
                case RIGHT:
                    model.setDirection(Direction.DOWN);
                    break;
                case LEFT:
                    model.setDirection(Direction.UP);
                    break;
            }
        }
        if (key == KeyEvent.VK_LEFT) {
            switch (model.getDirection()) {
                case UP:
                    model.setDirection(Direction.LEFT);
                    break;
                case DOWN:
                    model.setDirection(Direction.RIGHT);
                    break;
                case RIGHT:
                    model.setDirection(Direction.UP);
                    break;
                case LEFT:
                    model.setDirection(Direction.DOWN);
                    break;
            }
        }
    }
}
